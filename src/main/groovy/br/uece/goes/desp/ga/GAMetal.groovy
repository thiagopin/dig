package br.uece.goes.desp.ga

import br.uece.goes.gen.Instance
import br.uece.goes.opt.Result
import br.uece.goes.sample.Bug
import org.apache.commons.math3.genetics.*
import org.apache.commons.math3.random.MersenneTwister

class GAMetal<T> {

    private final int POP_SIZE = 100
    private final int NUM_BUGS = 30

    private final Instance<T> instance
    private final int fase
    private final List<List<Integer>> seed
    private BugEvaluator evaluator

    public GAMetal(Instance<T> instance, List<List<Integer>> seed, int fase) {
        this.instance = instance
        this.seed = seed
        this.fase = fase
        this.evaluator = new BugEvaluator(instance.data)
    }

    public GeneticAlgorithm builder(double highMutationRate, double lowMutationRate) {
        def ga = new GeneticAlgorithmCallback(
                new OrderedCrossover<Integer>(), 1,
                new BugMutation(instance.size(), highMutationRate, lowMutationRate), 1,
                new TournamentSelection(5)
        )
        ga.randomGenerator = new MersenneTwister()
        ga
    }

    public void solve(double highMutationRate, double lowMutationRate, int time, Closure callback) {
        def cond = new FixedElapsedTime(time)
        def pop = initialPop()

        def ga = builder(highMutationRate, lowMutationRate)
        def top = topScore(this.evaluator, instance.data)

        ga.evolve(pop, cond) { Population current, int gen ->
            ((BugMutation) ga.mutationPolicy).increment()
            Result<Bug> result = createResult(current, gen, fase, top)
            callback(result)
        }
    }

    public Population initialPop(List<List<Integer>> seed = []) {
        if (seed.size() > instance.data.size())
            throw new IllegalArgumentException("Seed must contains less elements than the instance.")
        def rest = POP_SIZE - seed.size()

        def chromosomes = seed.collect { new CombinatorialChromosome(it, evaluator) } +
                (0..<rest).collect {
                    new CombinatorialChromosome(instance.data.indices.rand(NUM_BUGS, false), evaluator)
                }

        new ElitisticListPopulation(chromosomes, POP_SIZE, 0.01)
    }

    private Result<Bug> createResult(final Population current, final int generation, final int fase, final double top) {
        def chromosome = (CombinatorialChromosome) current.fittestChromosome
        def stats = ((ElitisticListPopulation) current).chromosomes*.fitness.average()
        new Result<Bug>(chromosome.fitness, [], [gen : generation, fase: fase,
                                                 pop : chromosome.data.collect { instance.data[it] },
                                                 mean: stats.mean, std: stats.stdDev, top: top])
    }

    private double topScore(BugEvaluator evaluator, List<Bug> bugs) {
        (double) (0..<bugs.size()).collect { Integer it -> evaluator.bugScore(bugs[it], it) }
                .sort(false) { double it -> -it }.take(NUM_BUGS).sum()
    }
}
