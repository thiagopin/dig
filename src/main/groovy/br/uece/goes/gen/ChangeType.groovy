package br.uece.goes.gen

enum ChangeType {
    DIMENSIONAL, NON_DIMENSIONAL, RANDOM
}