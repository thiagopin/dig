package br.uece.goes.opt

import groovy.transform.CompileStatic
import groovy.transform.Immutable

@Immutable
@CompileStatic
class Result<T> {
    BigDecimal value
    List<T> state
    Map<String, ? extends Object> props
}
