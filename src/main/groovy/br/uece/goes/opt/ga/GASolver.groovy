package br.uece.goes.opt.ga

import br.uece.goes.gen.Instance
import br.uece.goes.opt.Result
import br.uece.goes.opt.Solver
import br.uece.goes.opt.generator.GaussGenerator
import br.uece.goes.sample.Bug
import groovy.transform.TypeChecked
import org.uncommons.maths.random.MersenneTwisterRNG
import org.uncommons.maths.random.Probability
import org.uncommons.watchmaker.framework.PopulationData
import org.uncommons.watchmaker.framework.operators.EvolutionPipeline
import org.uncommons.watchmaker.framework.operators.ListOrderCrossover
import org.uncommons.watchmaker.framework.operators.Replacement
import org.uncommons.watchmaker.framework.selection.RouletteWheelSelection
import org.uncommons.watchmaker.framework.termination.UserAbort
import rx.Observable
import rx.Subscriber
import rx.schedulers.Schedulers
import rx.subscriptions.Subscriptions

@TypeChecked
class GASolver<T> implements Solver {

    private final Instance<T> instance
    private final int numBugs
    private final int fase
    private final List<List<Integer>> seed

    public GASolver(Instance<T> instance, int numBugs, int fase) {
        this(instance, numBugs, [], fase)
    }

    public GASolver(Instance<T> instance, int numBugs, List<List<Integer>> seed, int fase) {
        this.instance = instance
        this.numBugs = numBugs
        this.seed = seed
        this.fase = fase
    }

    private double topScore(BugEvaluator evaluator, List<Bug> bugs) {
        (double) (0..<bugs.size()).collect { Integer it -> evaluator.bugScore(bugs[it], it) }
                .sort(false) { double it -> -it }.take(numBugs).sum()
    }

    private void createSubscription(final Subscriber<Result<T>> subscriber,
                                    double alpha, double beta, double gamma,
                                    double highMutationRate = 0.6, double lowMutationRate = 0.05) {
        Random rng = new MersenneTwisterRNG()
        final UserAbort termination = new UserAbort()
        final int populationSize = 100

        def evaluator = new BugEvaluator(instance.data as List<Bug>, alpha, beta, gamma)
        def top = topScore(evaluator, (List<Bug>) instance.data)

        def generator = new GaussGenerator(highMutationRate, lowMutationRate, 4, 0.0002)

        def cFactory = new LimitPermutationFactory<>((0..<instance.size()), numBugs)

        def pipeline = new EvolutionPipeline([
                new ListOrderCrossover<Integer>(),
                new BugMutation((0..<instance.size()) as IntRange, generator),
                new Replacement<List<Integer>>(cFactory, new Probability(0.15))])

        def engine = new PopGenerationalEvolutionEngine<>(cFactory, pipeline,
                evaluator, new RouletteWheelSelection(), rng)

        engine.addPopEvolutionObserver(new PopEvolutionObserver<List<Integer>>() {
            @Override
            void populationUpdate(PopulationData<? extends List<Integer>> data, List<? extends List<Integer>> pop) {
                generator.increment()
                if (!subscriber.isUnsubscribed() && data != null) {
                    subscriber.onNext(new Result<T>(BigDecimal.valueOf(data.bestCandidateFitness),
                            data.bestCandidate.collect { Integer el -> instance.data[el] },
                            [pop : pop, mean: data.meanFitness, std: data.fitnessStandardDeviation, top: top,
                             fase: fase, gen: data.generationNumber]))
                }
            }
        })
        subscriber.add(Subscriptions.create {
            termination.abort()
        })
        long init = System.currentTimeMillis()
        try {
            println "iniciando ${Thread.currentThread()}"
            engine.evolve(populationSize, 1, seed, termination)
        } catch (Throwable err) {
            subscriber.onError(err)
        } finally {
            println "finalizando ${Thread.currentThread()} depois de ${System.currentTimeMillis() - init} ms"
            subscriber.onCompleted()
        }
    }

    public Observable<Result<T>> solve(double alpha, double beta, double gamma,
                                       double highMutationRate, double lowMutationRate) {
        Observable.create({ final Subscriber<Result<T>> subscriber ->
            createSubscription(subscriber, alpha, beta, gamma, highMutationRate, lowMutationRate)
        } as Observable.OnSubscribe<Result<T>>).subscribeOn(Schedulers.newThread())
    }

    @Override
    public Observable<Result<T>> solve(Map<String, ? extends Object> params) {
        solve((Double) params['alpha'], (Double) params['beta'], (Double) params['gamma'],
                (Double) params['highMutationRate'], (Double) params['lowMutationRate'])
    }
}
