package br.uece.goes

import org.knowm.xchart.*
import org.knowm.xchart.internal.chartpart.Chart
import org.knowm.xchart.internal.style.Styler

import javax.swing.*
import java.awt.*
import java.util.List
import java.util.concurrent.atomic.AtomicBoolean

class MainChart {
    final String SERIES_NAME = "Evolução"
    final String TOP_SOLUTION = "Ótimo"
    final String MEAN_SERIES = "Média Pop."
    final String STD_SERIES = "Desv Pad."

    def xdata = [0] as Vector
    def xdata2 = [0] as Vector
    def ydata = [0] as Vector
    def tdata = [0] as Vector
    def mdata = [0] as Vector
    def sdata = [0] as Vector
    def first = new AtomicBoolean(true)

    public MainChart(rx.Observable<Map<String, ? extends Object>> obs) {
        def frame = new JFrame("DIG Chart")
        frame.defaultCloseOperation = JFrame.EXIT_ON_CLOSE
//        frame.layout = new FlowLayout()
//        frame.preferredSize = new Dimension(800, 600)

        def mainPanel = new XChartPanel(buildChart(xdata, [ydata, tdata, mdata], [SERIES_NAME, TOP_SOLUTION, MEAN_SERIES]))
//        def statPanel = new XChartPanel(buildChart(xdata2, [mdata, sdata], [MEAN_SERIES, STD_SERIES], false))

        obs.subscribe {
            updateChart(mainPanel, it, ['value', 'top', 'mean'], xdata, [ydata, tdata, mdata], [SERIES_NAME, TOP_SOLUTION, MEAN_SERIES])
//            updateChart(statPanel, it, ['mean', 'std'], xdata2, [mdata, sdata], [MEAN_SERIES, STD_SERIES])
        }
        frame.add(mainPanel)
//        frame.add(statPanel)

        frame.pack()
        frame.locationRelativeTo = null
        frame.visible = true
    }

    synchronized void updateChart(XChartPanel panel, Map values, List<String> keys,
                                  List<Long> xs, List<List<Double>> ys, List<String> labels) {
        synchronized (panel) {
            if (first.get() || values.time < xs.last()) {
                xs.clear()
                ys.each { it.clear() }
                first.set(false)
            }
            xs << values.time
            (0..<ys.size()).each { idx ->
                ys[idx] << values[keys[idx]]
                try {
                    panel.updateSeries(labels[idx], xs, ys[idx], null)
                } catch (NoSuchElementException | ConcurrentModificationException e) {
                    println 'ooops'
                }
            }
        }
    }

    Chart<Styler_XY, Series_XY> buildChart(List<Long> xs, List<List<Double>> ys, List<String> labels, boolean big = true) {
        def (width, height) = big ? [800, 600] : [800, 200]
        Chart_XY chart = new ChartBuilder_XY().width(width).height(height).theme(Styler.ChartTheme.Matlab)
                .title(big ? "Evolução" : "Stats").xAxisTitle("Tempo (s)").yAxisTitle("Fitness").build()
        chart.styler.setPlotGridLinesVisible(false)
        chart.styler.setXAxisTickMarkSpacingHint(100)
        (0..<labels.size()).each { idx ->
            chart.addSeries(labels[idx], xs, ys[idx])
        }
        chart
    }

}
