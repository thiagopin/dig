package br.uece.goes.opt.ga

import br.uece.goes.sample.Bug
import groovy.transform.CompileStatic
import org.uncommons.watchmaker.framework.FitnessEvaluator

@CompileStatic
class BugEvaluator implements FitnessEvaluator<List<Integer>> {

    private List<Bug> bugs
    private List<Double> votes
    private double alpha, beta, gamma

    public BugEvaluator(List<Bug> bugs, Double alpha, Double beta, Double gamma) {
        this.bugs = bugs.asImmutable()
        this.votes = normVotes(bugs)
        this.alpha = alpha
        this.beta = beta
        this.gamma = gamma
    }

    List<Double> normVotes(List<Bug> bugs) {
        Bug min = bugs.min { it.votes }
        Bug max = bugs.max { it.votes }
        bugs.collect { ((it.votes - min.votes) / (max.votes - min.votes)).doubleValue() }
    }

    public double bugScore(Bug bug, int pos) {
        alpha * votes[pos] + beta * (bug.priority * (bugs.size() - pos + 1)) - gamma * (bug.severity * pos)
    }

    @Override
    double getFitness(List<Integer> candidate, List<? extends List<Integer>> population) {
        return candidate.inject(0.0) { acc, pos ->
            double total = bugScore(bugs[pos], pos)
            acc += total >= 0 ? total : 0
        } as Double
    }

    @Override
    boolean isNatural() {
        return true
    }
}
