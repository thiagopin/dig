package br.uece.goes.sample

import br.uece.goes.gen.Generator

public class BugGenerator extends Generator<Bug> {

    private long counter = 1

    //Lista com valores para severidade
    List<Double> severities = [0.1, 0.25, 0.4, 0.55, 0.70, 0.85, 1.0].asImmutable()

    //Lista com valores para prioridade
    List<Double> priorities = [0.2, 0.4, 0.6, 0.8, 1.0].asImmutable()

    //Lista com valores para prioridade
    Range<Integer> votes = 1..8

    @Override
    Bug generate() {
        new Bug(id: counter++,
                severity: severities.rand(),
                priority: priorities.rand(),
                votes: votes.rand())
    }

    @Override
    Bug update(Bug elem) {
        new Bug(id: elem.id,
                severity: severities.rand(),
                priority: priorities.rand(),
                votes: elem.votes + votes.rand())
    }
}
