package br.uece.goes.mediator

import groovy.transform.CompileStatic
import rx.Subscription
import rx.subscriptions.Subscriptions

@CompileStatic
public class Tup {
    private final Long seq
    private final List<List<Integer>> pop
    private final Subscription subscription

    public Tup(Long seq, List<List<Integer>> pop, Subscription subscription) {
        this.seq = seq
        this.pop = pop
        this.subscription = subscription
    }

    public Tup() {
        this(1L, [], Subscriptions.empty())
    }

    public synchronized List<List<Integer>> getPop() {
        pop
    }

    public synchronized void setPop(List<List<Integer>> pop) {
        this.pop.clear()
        this.pop.addAll(pop)
    }

    public Subscription getSubscription() {
        subscription
    }

    public Long getSeq() {
        seq
    }

}