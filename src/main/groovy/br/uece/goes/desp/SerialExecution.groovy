package br.uece.goes.desp

import br.uece.goes.Main
import br.uece.goes.gen.Instance
import br.uece.goes.opt.Result
import br.uece.goes.sample.Bug
import br.uece.goes.sample.BugDataSet
import groovy.transform.TypeChecked
import org.codehaus.groovy.reflection.ReflectionUtils

import java.nio.file.Files
import java.nio.file.Path
import java.nio.file.Paths

class SerialExecution {
    public static void exec() {
        def params = [[[1.0], [0.3, 0.6, 0.9]], [[0.3, 0.6, 0.9], [0.05]]].collectMany {
            ([['low', 'medium', 'high'], [30, 60, 90]] + it).combinations()
        }
//        def params = [['low', 15, 1.0, 0.3], ['medium', 15, 0.6, 0.05]] // sample
        def EXECS = 30

        def home = new File("${System.getProperty('user.home')}/dig-data")
        def target = home.exists() ? home.absolutePath : System.getProperty('java.io.tmpdir')

        params.each { args ->
            def path = Paths.get("${target}/${args.join('-')}.csv")
            Files.deleteIfExists(path)

            path.write(['id', 'params', 'fase', 'gen', 'time', 'value', 'mean', 'std', 'best'].join(';') + '\n')
            EXECS.times {
                batchSimulation(*(args + [path]))
            }

        }
    }

    @TypeChecked
    public static void batchSimulation(String changeImpact, int duration, float popPercencual,
                                       float highMutationRate, Path file) {
        final String id = UUID.randomUUID()
        final String params = [changeImpact, duration, popPercencual, highMutationRate].join('-')

        List<Instance<Bug>> changes = createInstancesWithin(changeImpact)

        long initial = System.currentTimeMillis()
        changes.indices.inject([]) { List<List<Integer>> seed, int idx ->
            List<List<Integer>> pop = []
            optimize(changes[idx], seed, idx + 1, highMutationRate, duration) { Result<Bug> res ->
                String line = [id, params, res.props['fase'], res.props['gen'], (System.currentTimeMillis() - initial),
                               String.format('%.5f', res.value), String.format('%.5f', res.props['mean']),
                               String.format('%.5f', res.props['std']), String.format('%.5f', res.props['top'])].join(';')
                pop = nextPopSize(res.props['pop'] as List<List<Integer>>, popPercencual)
                file << (line + '\n')
            }
            pop
        }
    }

    @TypeChecked
    public static List<List<Integer>> nextPopSize(List<List<Integer>> pop, float perc) {
        int top = (int) (pop.size() * perc)
        pop.take(top)
    }

    @TypeChecked
    public static List<Instance<Bug>> createInstancesWithin(String changeImpact) {
        def instances = Main.createInstances(changeImpact)
        def bugs = new BugDataSet()

        (0..<instances.size()).collect {
            bugs.read(ReflectionUtils.getCallingClass(0).getResourceAsStream(instances[(int) it]))
        }.take(instances.size() + 1)
    }

    @TypeChecked
    public static void optimize(Instance<Bug> instance, List<List<Integer>> seed, int fase,
                                float highMutationRate, int seconds, Closure cback) {
        final float lowMutationRate = 0.05
        new GADesp<Bug>(instance, seed, fase).solve(highMutationRate, lowMutationRate, seconds, cback)
    }
}
