package br.uece.goes.opt.ga;

import org.uncommons.maths.statistics.DataSet;
import org.uncommons.watchmaker.framework.*;

import java.util.*;
import java.util.concurrent.CopyOnWriteArraySet;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

public class PopGenerationalEvolutionEngine<T> implements EvolutionEngine<T> {

    // A single multi-threaded worker is shared among multiple evolution engine instances.
    private static FitnessEvaluationWorker concurrentWorker = null;

    private final Set<PopEvolutionObserver<? super T>> popObservers = new CopyOnWriteArraySet<PopEvolutionObserver<? super T>>();
    private final EvolutionaryOperator<T> evolutionScheme;
    private final FitnessEvaluator<? super T> fitnessEvaluator;
    private final SelectionStrategy<? super T> selectionStrategy;
    private final Random rng;
    private final CandidateFactory<T> candidateFactory;

    private volatile boolean singleThreaded = false;
    private List<TerminationCondition> satisfiedTerminationConditions;

    public PopGenerationalEvolutionEngine(CandidateFactory<T> candidateFactory, EvolutionaryOperator<T> evolutionScheme,
                                          FitnessEvaluator<? super T> fitnessEvaluator,
                                          SelectionStrategy<? super T> selectionStrategy, Random rng) {
        this.candidateFactory = candidateFactory;
        this.fitnessEvaluator = fitnessEvaluator;
        this.rng = rng;
        this.evolutionScheme = evolutionScheme;
        this.selectionStrategy = selectionStrategy;
    }

    private List<EvaluatedCandidate<T>> nextEvolutionStep(List<EvaluatedCandidate<T>> evaluatedPopulation,
                                                          int eliteCount,
                                                          Random rng) {
        List<T> population = new ArrayList<T>(evaluatedPopulation.size());

        // First perform any elitist selection.
        List<T> elite = new ArrayList<T>(eliteCount);
        Iterator<EvaluatedCandidate<T>> iterator = evaluatedPopulation.iterator();
        while (elite.size() < eliteCount) {
            elite.add(iterator.next().getCandidate());
        }
        // Then select candidates that will be operated on to create the evolved
        // portion of the next generation.
        population.addAll(selectionStrategy.select(evaluatedPopulation, fitnessEvaluator.isNatural(),
                evaluatedPopulation.size() - eliteCount, rng));
        // Then evolve the population.
        population = evolutionScheme.apply(population, rng);
        // When the evolution is finished, add the elite to the population.
        population.addAll(elite);
        return evaluatePopulation(population);

    }

    public T evolve(int populationSize, int eliteCount, TerminationCondition... conditions) {
        return evolve(populationSize, eliteCount, Collections.<T>emptySet(), conditions);
    }

    public T evolve(int populationSize, int eliteCount, Collection<T> seedCandidates,
                    TerminationCondition... conditions) {
        return evolvePopulation(populationSize, eliteCount, seedCandidates, conditions).get(0).getCandidate();
    }

    public List<EvaluatedCandidate<T>> evolvePopulation(int populationSize, int eliteCount,
                                                        TerminationCondition... conditions) {
        return evolvePopulation(populationSize, eliteCount, Collections.<T>emptySet(), conditions);
    }

    public List<EvaluatedCandidate<T>> evolvePopulation(int populationSize,
                                                        int eliteCount,
                                                        Collection<T> seedCandidates,
                                                        TerminationCondition... conditions) {
        if (eliteCount < 0 || eliteCount >= populationSize) {
            throw new IllegalArgumentException("Elite count must be non-negative and less than population size.");
        }
        if (conditions.length == 0) {
            throw new IllegalArgumentException("At least one TerminationCondition must be specified.");
        }

        satisfiedTerminationConditions = null;
        int currentGenerationIndex = 0;
        long startTime = System.currentTimeMillis();

        List<T> population = candidateFactory.generateInitialPopulation(populationSize,
                seedCandidates,
                rng);

        // Calculate the fitness scores for each member of the initial population.
        List<EvaluatedCandidate<T>> evaluatedPopulation = evaluatePopulation(population);
        PopulationData<T> data = calcPopData(evaluatedPopulation, eliteCount, currentGenerationIndex, startTime);

        List<TerminationCondition> satisfiedConditions = EvolutionUtils.shouldContinue(data, conditions);
        while (satisfiedConditions == null) {
            ++currentGenerationIndex;
            evaluatedPopulation = nextEvolutionStep(evaluatedPopulation, eliteCount, rng);
            data = calcPopData(evaluatedPopulation, eliteCount, currentGenerationIndex, startTime);
            // Notify observers of the state of the population.
            satisfiedConditions = EvolutionUtils.shouldContinue(data, conditions);
        }
        this.satisfiedTerminationConditions = satisfiedConditions;
        return evaluatedPopulation;
    }

    public PopulationData<T> calcPopData(List<EvaluatedCandidate<T>> evaluatedPopulation, int eliteCount,
                                         int currentGenerationIndex, long startTime) {
        EvolutionUtils.sortEvaluatedPopulation(evaluatedPopulation, fitnessEvaluator.isNatural());
        PopulationData<T> data = EvolutionUtils.getPopulationData(evaluatedPopulation,
                fitnessEvaluator.isNatural(), eliteCount, currentGenerationIndex, startTime);
        // Notify observers of the state of the population.
        notifyPopulationChange(data, candidates(evaluatedPopulation));
        return data;
    }

    private List<T> candidates(List<EvaluatedCandidate<T>> pop) {
        List<T> aux = new ArrayList<T>();
        for (EvaluatedCandidate<T> cand : pop) {
            aux.add(cand.getCandidate());
        }
        return aux;
    }

    protected List<EvaluatedCandidate<T>> evaluatePopulation(List<T> population) {
        List<EvaluatedCandidate<T>> evaluatedPopulation = new ArrayList<EvaluatedCandidate<T>>(population.size());

        if (singleThreaded) // Do fitness evaluations on the request thread.
        {
            for (T candidate : population) {
                evaluatedPopulation.add(new EvaluatedCandidate<T>(candidate,
                        fitnessEvaluator.getFitness(candidate, population)));
            }
        } else {
            // Divide the required number of fitness evaluations equally among the
            // available processors and coordinate the threads so that we do not
            // proceed until all threads have finished processing.
            try {
                List<T> unmodifiablePopulation = Collections.unmodifiableList(population);
                List<Future<EvaluatedCandidate<T>>> results = new ArrayList<Future<EvaluatedCandidate<T>>>(population.size());
                // Submit tasks for execution and wait until all threads have finished fitness evaluations.
                for (T candidate : population) {
                    results.add(getSharedWorker().submit(new FitnessEvalutationTask<T>(fitnessEvaluator,
                            candidate, unmodifiablePopulation)));
                }
                for (Future<EvaluatedCandidate<T>> result : results) {
                    evaluatedPopulation.add(result.get());
                }
            } catch (ExecutionException ex) {
                throw new IllegalStateException("Fitness evaluation task execution failed.", ex);
            } catch (InterruptedException ex) {
                // Restore the interrupted status, allows methods further up the call-stack
                // to abort processing if appropriate.
                Thread.currentThread().interrupt();
            }
        }

        return evaluatedPopulation;
    }

    public List<TerminationCondition> getSatisfiedTerminationConditions() {
        if (satisfiedTerminationConditions == null) {
            throw new IllegalStateException("EvolutionEngine has not terminated.");
        } else {
            return Collections.unmodifiableList(satisfiedTerminationConditions);
        }
    }

    public void notifyPopulationChange(PopulationData<T> data, List<T> population) {
        for (PopEvolutionObserver<? super T> observer : popObservers) {
            observer.populationUpdate(data, population);
        }
    }

    public void addPopEvolutionObserver(PopEvolutionObserver<? super T> observer) {
        popObservers.add(observer);
    }

    public void removePopEvolutionObserver(PopEvolutionObserver<? super T> observer) {
        popObservers.remove(observer);
    }

    public void addEvolutionObserver(EvolutionObserver<? super T> observer) {
        throw new UnsupportedOperationException();
    }

    public void removeEvolutionObserver(EvolutionObserver<? super T> observer) {
        throw new UnsupportedOperationException();
    }

    public void setSingleThreaded(boolean singleThreaded) {
        this.singleThreaded = singleThreaded;
    }

    private static synchronized FitnessEvaluationWorker getSharedWorker() {
        if (concurrentWorker == null) {
            concurrentWorker = new FitnessEvaluationWorker();
        }
        return concurrentWorker;
    }

}
