package br.uece.goes.opt.generator

import org.uncommons.maths.number.NumberGenerator
import org.uncommons.maths.random.Probability

import java.util.concurrent.atomic.AtomicLong

class GaussGenerator implements NumberGenerator<Probability> {
    private final double init;
    private final double end;
    private AtomicLong genCounter = new AtomicLong(0L);
    private final double decayRate
    private final double genDecay

    /**
     *
     * @param init valor inicial assumido pela taxa de mutação.
     * @param end valor final a ser alcançado pela taxa de mutação.
     * @param genDecay a partir de qual geração a taxa de mutação deve iniciar sua queda.
     * @param decayRate valores entre 0 e 1, onde quanto menor mais suave a queda (valores perto de 1 tem queda abrupta).
     */
    public GaussGenerator(double init, double end, int genDecay, double decayRate) {
        //decayRate: 0.0043 -> 40 gerações de queda
        //decayRate: 0.0002 ~> 200 gerações de queda
        if (decayRate < 0 || decayRate > 1)
            throw new IllegalArgumentException('decayRate must be a value between 0 and 1.')
        if (genDecay < 0) throw new IllegalArgumentException('genDecay must be a value higher than 0.')
        this.init = init;
        this.end = end;
        this.decayRate = decayRate;
        this.genDecay = genDecay;
    }

    /**
     * @return The constant value specified when the generator was constructed.
     */
    public Probability nextValue() {
        long c = genCounter.get()
        return new Probability(gaussDecay(c));
    }

    public void increment() {
        genCounter.incrementAndGet()
    }

    private double gaussDecay(long gen) {
        double numerator = (Math.max(0, gen - genDecay)**2)
        end + (init - end) * Math.exp(-numerator * decayRate / 2)
    }
}