package br.uece.goes.desp.ga

import br.uece.goes.gen.Instance
import br.uece.goes.opt.Result
import br.uece.goes.opt.Solver
import br.uece.goes.sample.Bug
import groovy.transform.TypeChecked
import org.apache.commons.math3.genetics.*
import org.apache.commons.math3.random.MersenneTwister
import rx.Observable
import rx.Subscriber
import rx.schedulers.Schedulers
import rx.subscriptions.Subscriptions

@TypeChecked
class CommonGASolver<T> implements Solver {

    private final int POP_SIZE = 100

    private final Instance<T> instance
    private final int numBugs
    private final int fase
    private final List<List<Integer>> seed

    public CommonGASolver(Instance<T> instance, int numBugs, int fase) {
        this(instance, numBugs, [], fase)
    }

    public CommonGASolver(Instance<T> instance, int numBugs, List<List<Integer>> seed, int fase) {
        this.instance = instance
        this.numBugs = numBugs
        this.seed = seed
        this.fase = fase
    }

    private double topScore(BugEvaluator evaluator, List<Bug> bugs) {
        (double) (0..<bugs.size()).collect { Integer it -> evaluator.bugScore(bugs[it], it) }
                .sort(false) { double it -> -it }.take(numBugs).sum()
    }

    private void createSubscription(final Subscriber<Result<T>> subscriber,
                                    double alpha, double beta, double gamma,
                                    double highMutationRate = 0.6, double lowMutationRate = 0.05) {
        def evaluator = new BugEvaluator(instance.data as List<Bug>, alpha, beta, gamma)
        def top = topScore(evaluator, instance.data as List<Bug>)

        def ga = new GeneticAlgorithmCallback(
                new OrderedCrossover<Integer>(), 1,
                new BugMutation(instance.size(), highMutationRate, lowMutationRate), 1,
                new TournamentSelection(5)
        )
        ga.randomGenerator = new MersenneTwister()
        def cond = new UserStop()
        def pop = initialPop(evaluator, seed)

        subscriber.add(Subscriptions.create {
            cond.abort()
        })

        try {
            ga.evolve(pop, cond) { Population current, int gen ->
                ((BugMutation) ga.mutationPolicy).increment()
                Result<T> result = createResult(current, gen, fase, top)
                subscriber.onNext(result)
            }
        } catch (Throwable err) {
            subscriber.onError(err)
        } finally {
            subscriber.onCompleted()
        }
    }

    public Population initialPop(BugEvaluator evaluator, List<List<Integer>> seed = []) {
        if (seed.size() > instance.data.size())
            throw new IllegalArgumentException("Seed must contains less elements than the instance.")
        def rest = POP_SIZE - seed.size()

        def chromosomes = (seed.collect { new CombinatorialChromosome(it, evaluator) } +
                (0..<rest).collect {
                    new CombinatorialChromosome(instance.data.indices.rand(numBugs, false), evaluator)
                }) as List<Chromosome>

        new ElitisticListPopulation(chromosomes, POP_SIZE, 0.01)
    }

    private Result<T> createResult(final Population current, final int generation, final int fase, final double top) {
        def chromosome = (CombinatorialChromosome) current.fittestChromosome
        def stats = ((ElitisticListPopulation) current).chromosomes*.fitness.average()
        def state = chromosome.data.collect { instance.data[it] } as List<T>

        new Result<T>(new BigDecimal(chromosome.fitness),
                state, [gen : generation, fase: fase,
                        pop : ((ElitisticListPopulation) current).chromosomes.collect {
                            ((CombinatorialChromosome) it).data
                        },
                        mean: stats.mean, std: stats.stdDev, top: top])
    }

    public Observable<Result<T>> solve(double alpha, double beta, double gamma,
                                       double highMutationRate, double lowMutationRate) {
        Observable.create({ final Subscriber<Result<T>> subscriber ->
            createSubscription(subscriber, alpha, beta, gamma, highMutationRate, lowMutationRate)
        } as Observable.OnSubscribe<Result<T>>).subscribeOn(Schedulers.newThread())
    }

    @Override
    public Observable<Result<T>> solve(Map<String, ? extends Object> params) {
        solve((Double) params['alpha'], (Double) params['beta'], (Double) params['gamma'],
                (Double) params['highMutationRate'], (Double) params['lowMutationRate'])
    }
}
