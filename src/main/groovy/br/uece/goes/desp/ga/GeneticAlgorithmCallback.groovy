package br.uece.goes.desp.ga

import org.apache.commons.math3.exception.OutOfRangeException
import org.apache.commons.math3.genetics.CrossoverPolicy
import org.apache.commons.math3.genetics.GeneticAlgorithm
import org.apache.commons.math3.genetics.MutationPolicy
import org.apache.commons.math3.genetics.Population
import org.apache.commons.math3.genetics.SelectionPolicy
import org.apache.commons.math3.genetics.StoppingCondition

class GeneticAlgorithmCallback extends GeneticAlgorithm {

    private int generationsEvolved = 0

    /**
     * Create a new genetic algorithm.
     * @param crossoverPolicy The {@link CrossoverPolicy}
     * @param crossoverRate The crossover rate as a percentage (0-1 inclusive)
     * @param mutationPolicy The {@link MutationPolicy}
     * @param mutationRate The mutation rate as a percentage (0-1 inclusive)
     * @param selectionPolicy The {@link SelectionPolicy}
     * @throws OutOfRangeException if the crossover or mutation rate is outside the [0, 1] range
     */
    public GeneticAlgorithmCallback(CrossoverPolicy crossoverPolicy, double crossoverRate, MutationPolicy mutationPolicy,
                             double mutationRate, SelectionPolicy selectionPolicy) throws OutOfRangeException {
        super(crossoverPolicy, crossoverRate, mutationPolicy, mutationRate, selectionPolicy)
    }

    /**
     * Evolve the given population. Evolution stops when the stopping condition
     * is satisfied. Updates the {@link #getGenerationsEvolved() generationsEvolved}
     * property with the number of generations evolved before the StoppingCondition
     * is satisfied.
     *
     * @param initial the initial, seed population.
     * @param condition the stopping condition used to stop evolution.
     * @param callback closure to add extra behavior at each new generation.
     * @return the population that satisfies the stopping condition.
     */
    public Population evolve(final Population initial, final StoppingCondition condition, Closure callback) {
        Population current = initial;
        generationsEvolved = 0;
        callback(current, generationsEvolved);
        while (!condition.isSatisfied(current)) {
            current = nextGeneration(current);
            generationsEvolved++;
            callback(current, generationsEvolved);
        }
        return current;
    }

    @Override
    int getGenerationsEvolved() {
        this.generationsEvolved
    }
}
