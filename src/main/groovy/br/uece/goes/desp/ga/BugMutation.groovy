package br.uece.goes.desp.ga

import br.uece.goes.opt.generator.GaussGenerator
import org.apache.commons.math3.exception.MathIllegalArgumentException
import org.apache.commons.math3.genetics.Chromosome
import org.apache.commons.math3.genetics.MutationPolicy
import org.uncommons.maths.number.NumberGenerator
import org.uncommons.maths.random.MersenneTwisterRNG
import org.uncommons.maths.random.Probability

class BugMutation implements MutationPolicy {
    private Random rng = new MersenneTwisterRNG()
    private NumberGenerator<Probability> generator
    private int qtBugs

    public BugMutation(int qtBugs, double highMutationRate, double lowMutationRate) {
        this.generator = new GaussGenerator(highMutationRate, lowMutationRate, 4, 0.0002)
        this.qtBugs = qtBugs
    }

    @Override
    Chromosome mutate(Chromosome original) throws MathIllegalArgumentException {
        def chromosome = (CombinatorialChromosome) original

        List<Integer> otherValues = ((0..<qtBugs).toSet() - chromosome.data.toSet()).toList().rand(chromosome.length, false, rng)
        chromosome.data.indexed().collect { k, v ->
            if (generator.nextValue().nextEvent(rng)) otherValues[k] else v
        }


        return chromosome
    }

    public void increment() {
        ((GaussGenerator) generator).increment()
    }
}
