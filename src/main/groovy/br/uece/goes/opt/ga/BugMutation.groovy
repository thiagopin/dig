package br.uece.goes.opt.ga

import groovy.transform.CompileStatic
import org.uncommons.maths.number.ConstantGenerator
import org.uncommons.maths.number.NumberGenerator
import org.uncommons.maths.random.Probability
import org.uncommons.watchmaker.framework.EvolutionaryOperator

@CompileStatic
class BugMutation implements EvolutionaryOperator<List<Integer>> {

    private final NumberGenerator<Probability> mutationProbability
    private IntRange qtBugs

    public BugMutation(IntRange qtBugs, Probability mutationProbability) {
        this(qtBugs, new ConstantGenerator<Probability>(mutationProbability))
    }

    public BugMutation(IntRange qtBugs,
                       NumberGenerator<Probability> mutationProbability) {
        this.qtBugs = qtBugs
        this.mutationProbability = mutationProbability;
    }

    @Override
    public List<List<Integer>> apply(List<List<Integer>> selectedCandidates, Random rng) {
        selectedCandidates.collect { mutateOrder(it, rng) }
    }

    private List<Integer> mutateOrder(List<Integer> s, Random rng) {
        List<Integer> otherValues = (qtBugs.toSet() - s.toSet()).toList().rand(s.size(), false, rng)
        s.indexed().collect { k, v ->
            if (mutationProbability.nextValue().nextEvent(rng)) otherValues[k] else v
        }
        /*s.indices.inject(new ArrayList<Integer>(s)) { acc, val ->
            def rand = (qtBugs.toSet() - acc.toSet()).toList().rand(1, false, rng).first()
            acc[val] = rand
            acc
        } as List<Integer>*/
    }
}
