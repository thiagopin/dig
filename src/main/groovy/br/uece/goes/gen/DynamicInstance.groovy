package br.uece.goes.gen

import groovy.transform.TypeChecked
import rx.Observable
import rx.functions.Func2

import java.util.concurrent.TimeUnit

@TypeChecked
public class DynamicInstance<T> {

    private final Random rand = new Random()
    private final Closure<Instance<T>> dimensional = { Instance<T> a, Instance<T> b ->
        rand.nextGaussian() >= 0.5 ? a.decreaseInstance() : a.increaseInstance()
    }
    private final Closure<Instance<T>> nonDimensional = { Instance<T> a, Instance<T> b -> a.mutateInstance() }
    private final Closure<Instance<T>> random = { Instance<T> a, Instance<T> b ->
        rand.nextGaussian() >= 0.5 ? dimensional(a, b) : nonDimensional(a, b)
    }

    private final Instance<T> inst
    private final ChangeType changeType
    private final Map<ChangeType, Closure<Instance<T>>> actions

    /**
     * Inicia uma instância dinâmica com uma instância inicial e que tipo de mudanças serão efetuadas nesta instância.
     * @param inst instância inicial.
     * @param changeType tipo de mudança (dimensional ou não dimensional).
     */
    public DynamicInstance(Instance<T> inst, ChangeType changeType) {
        this.inst = inst
        this.changeType = changeType
        this.actions = [(ChangeType.DIMENSIONAL)    : dimensional,
                        (ChangeType.NON_DIMENSIONAL): nonDimensional,
                        (ChangeType.RANDOM)         : random]
    }

    /**
     * Cria observavel intervalado com período de tempo fixo.
     * @param interval período de tempo (em segundos).
     * @return observavel intervalado.
     */
    public Observable<Instance<T>> makeObservable(int interval) {
        Observable.interval(0L, interval, TimeUnit.SECONDS)
                .flatMap { Observable.just(inst) }
                .scan(actions[changeType] as Func2)
    }

    /**
     * Cria observavel intervalado com período de tempo variável.
     * @param minInterval período de tempo mínimo (em segundos)
     * @param maxInterval período de tempo máximo (em segundos)
     * @return observavel intervalado.
     */
    public Observable<Instance<T>> makeObservable(int minInterval, int maxInterval) {
        if (maxInterval < minInterval)
            throw new IllegalArgumentException("max ($maxInterval) is less than min ($minInterval)")
        Observable.just(inst).repeat().concatMap {
            int inter = minInterval + rand.nextInt(maxInterval - minInterval)
            Observable.interval(inter, TimeUnit.SECONDS).map { el -> it }.take(1)
        }.scan(actions[changeType] as Func2)
    }
}
