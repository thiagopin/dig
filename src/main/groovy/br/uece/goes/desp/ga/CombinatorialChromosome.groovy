package br.uece.goes.desp.ga

import groovy.transform.CompileStatic
import org.apache.commons.math3.genetics.AbstractListChromosome
import org.apache.commons.math3.genetics.InvalidRepresentationException

@CompileStatic
public class CombinatorialChromosome extends AbstractListChromosome<Integer> {

    private BugEvaluator evaluator

    public CombinatorialChromosome(List<Integer> data, BugEvaluator evaluator) {
        super(data, false)
        this.evaluator = evaluator
    }

    @Override
    double fitness() {
        return evaluator.getFitness(representation)
    }

    @Override
    AbstractListChromosome<Integer> newFixedLengthChromosome(List<Integer> chromosomeRepresentation) {
        return new CombinatorialChromosome(chromosomeRepresentation.take(representation.size()), evaluator)
    }

    @Override
    protected void checkValidity(List<Integer> chromosomeRepresentation) throws InvalidRepresentationException {

    }

    public List<Integer> getData() {
        representation
    }
}
