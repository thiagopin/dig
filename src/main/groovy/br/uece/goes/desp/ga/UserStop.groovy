package br.uece.goes.desp.ga

import org.apache.commons.math3.genetics.Population
import org.apache.commons.math3.genetics.StoppingCondition

class UserStop implements StoppingCondition {

    private volatile boolean aborted = false

    @Override
    boolean isSatisfied(Population population) {
        return isAborted()
    }

    public void abort() {
        aborted = true
    }

    public boolean isAborted() {
        aborted
    }

    public void reset() {
        aborted = false;
    }
}
