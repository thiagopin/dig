package br.uece.goes.opt.ga;

import org.uncommons.watchmaker.framework.PopulationData;

import java.util.List;

public interface PopEvolutionObserver<T> {

    void populationUpdate(PopulationData<? extends T> data, List<? extends T> population);
}
