package br.uece.goes.mediator

import br.uece.goes.desp.ga.CommonGASolver
import br.uece.goes.gen.Instance
import br.uece.goes.opt.Result
import br.uece.goes.sample.Bug
import groovy.transform.TypeChecked
import rx.Observable
import rx.functions.Func1

import java.util.concurrent.atomic.AtomicLong

@TypeChecked
public class BugMediator<T extends Bug> {

    private final int NUM_BUGS = 30
    private final float ALPHA = 1.0
    private final float BETA = 1.0
    private final float GAMMA = 1.0
    private final float popPercentual
    private final float highMutationRate
    private final float lowMutationRate

    public BugMediator(float popPercentual, float highMutationRate, float lowMutationRate) {
        checkInput('popPercentual', popPercentual)
        checkInput('highMutationRate', highMutationRate)
        checkInput('lowMutationRate', lowMutationRate)
        this.popPercentual = popPercentual
        this.highMutationRate = highMutationRate
        this.lowMutationRate = lowMutationRate
    }

    public BugMediator() {
        this(0.1f, 0.6f, 0.05f)
    }

    void checkInput(String arg, float data) {
        if (data < 0.0f || data > 1.0f) throw new IllegalArgumentException("$arg must be between 0 and 1")
    }

    public Observable<Result<T>> solve(Instance<T> instance, List<List<Integer>> seed, Long seq) {
        float high = seq > 1 ? highMutationRate : lowMutationRate
        new CommonGASolver<T>(instance, NUM_BUGS, seed, seq.toInteger()).solve(ALPHA, BETA, GAMMA, high, lowMutationRate)
    }

    public Observable<Result<T>> mediator(Observable<Instance<T>> generator) {
        List<List<Integer>> pop = []
        AtomicLong seq = new AtomicLong(0L)

        generator.switchMap({ Instance<T> instance ->
            if (instance.data.isEmpty()) Observable.empty()
            else solve(instance, nextPopSize(pop, popPercentual), seq.incrementAndGet()).doOnNext({ Result<T> it ->
                synchronized (pop) {
                    pop.clear()
                    pop.addAll((List<List<Integer>>) it.props['pop'])
                }
            })
        } as Func1)
    }

    private List<List<Integer>> nextPopSize(List<List<Integer>> pop, float perc) {
        int top = (int) (pop.size() * perc)
        pop.take(top)
    }

}
