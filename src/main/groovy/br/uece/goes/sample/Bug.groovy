package br.uece.goes.sample

import groovy.transform.Immutable
import groovy.transform.ToString

@Immutable
@ToString(includePackage = false)
public class Bug {
    Long id
    Double severity, priority
    Integer votes

    Map<String, ? extends Number> asMap() {
        [id: id, severity: severity, priority: priority, votes: votes]
    }
}
