package br.uece.goes

import br.uece.goes.dataset.DataSet
import br.uece.goes.desp.SerialExecution
import br.uece.goes.gen.ChangeType
import br.uece.goes.gen.DynamicInstance
import br.uece.goes.gen.Instance
import br.uece.goes.mediator.BugMediator
import br.uece.goes.opt.Result
import br.uece.goes.opt.Solver
import br.uece.goes.opt.ga.GASolver
import br.uece.goes.sample.Bug
import br.uece.goes.sample.BugDataSet
import br.uece.goes.sample.BugGenerator
import com.hazelcast.core.Hazelcast
import com.hazelcast.core.HazelcastInstance
import com.hazelcast.core.ITopic
import groovy.io.FileType
import org.codehaus.groovy.reflection.ReflectionUtils
import rx.Observable
import rx.schedulers.Schedulers

import java.util.concurrent.TimeUnit

public class Main {
    public static void main(String[] args) {
        //gera instancias com modificações dimensionais e entre um periodo entre 3 a 5 segundos.
//        usage(ChangeType.NON_DIMENSIONAL, [3, 5])
//        optimize()
//        mediator(ChangeType.NON_DIMENSIONAL)

//        def (popPerc, highMut, lowMut) = args*.toFloat()
//        singleSimulation(popPerc, highMut, lowMut)
//        singleSimulation(0.1, 0.6, 0.05)

//        simpleExperimentExecution(Integer.parseInt(args[0]), args[1], Integer.parseInt(args[2]),
//                Float.parseFloat(args[3]), Float.parseFloat(args[4]))

//        simpleExperimentExecution(1, 'low', 15, 0.3, 0.05)
//        experimentExecution()
        SerialExecution.exec()
    }

    public static void simpleExperimentExecution(int exec, String changeImpact, int duration, float popPercencual,
                                                 float highMutationRate) {
        def home = new File("${System.getProperty('user.home')}/dig-data")
        def target = home.exists() ? home.absolutePath : System.getProperty('java.io.tmpdir')

        def file = new File("${target}/${[changeImpact, duration, popPercencual, highMutationRate, exec].join('-')}.csv")
        if (!file.exists()) file.delete()
        file << (['id', 'params', 'fase', 'gen', 'time', 'value', 'mean', 'std', 'best'].join(';') + '\n')

        batchSimulation(changeImpact, duration, popPercencual, highMutationRate)
                .toBlocking()//.subscribe { line -> println line }
                .subscribe { line -> file << (line + '\n') }

        println "Fim ${file.name}!!"
        System.exit(0)
    }

    public static void experimentExecution() {
        def params = [[[1.0], [0.3, 0.6, 0.9]], [[0.3, 0.6, 0.9], [0.05]]].collectMany {
            ([['low', 'medium', 'high'], [30, 60, 90]] + it).combinations()
        }
        //def params = [['low', 15, 1.0, 0.3], ['medium', 15, 0.6, 0.05]] // sample
        def EXECS = 30

        def home = new File("${System.getProperty('user.home')}/dig-data")
        def target = home.exists() ? home.absolutePath : System.getProperty('java.io.tmpdir')
        def executed = listExistingCombinations(target)
        def nonExecuted = params.findAll { !(it.join('-') in executed) }

//        synchronized(this) { this.wait()}

        nonExecuted.each { args ->
            def file = new File("${target}/${args.join('-')}.csv")
            if (file.exists()) file.delete()
            file << (['id', 'params', 'fase', 'gen', 'time', 'value', 'mean', 'std', 'best'].join(';') + '\n')
            EXECS.times {
                batchSimulation(*args).toBlocking()
//                        .subscribe { line -> println line }
                        .subscribe { line -> file << (line + '\n') }
            }

        }

    }

    public static Set<String> listExistingCombinations(String path) {
        def combs = []
        new File(path).eachFile(FileType.FILES) {
            if (it.name.endsWith('.csv')) combs << it.name[0..<it.name.indexOf('.csv')]
        }
        combs as Set
    }

    public static List<String> createInstances(String changeImpact) {
        ['/kate.csv', '/kate-2.csv', '/kate-3.csv', '/kate-4.csv', '/kate-5.csv'].collect {
            "/${changeImpact}${it}".toString()
        }
    }

    public static Observable<Instance<Bug>> createInstancesWithin(String changeImpact, int secondsDuration) {
        def instances = createInstances(changeImpact)
        def bugs = new BugDataSet()

        Observable.interval(0, secondsDuration, TimeUnit.SECONDS).map {
            if (it < instances.size())
                bugs.read(ReflectionUtils.getCallingClass(0).getResourceAsStream(instances[(int) it]))
            else
                new Instance<Bug>([], new BugGenerator())
        }.take(instances.size() + 1)
    }

    public static Observable<Result<Bug>> batchSimulation(String changeImpact, int duration, float popPercencual,
                                                          float highMutationRate) {
        final String id = UUID.randomUUID()
        final String params = [changeImpact, duration, popPercencual, highMutationRate].join('-')
        final float lowMutationRate = 0.05

        def mediator = new BugMediator<>(popPercencual, highMutationRate, lowMutationRate)
        def changes = createInstancesWithin(changeImpact, duration)

        def solutions = mediator.mediator(changes)
        def initial = System.currentTimeMillis()
        def en = Locale.US
        solutions.map { Result<Bug> res ->
            [id, params, res.props['fase'], res.props['gen'], (System.currentTimeMillis() - initial),
             String.format(en, '%.5f', res.value), String.format(en, '%.5f', res.props['mean']),
             String.format(en, '%.5f', res.props['std']), String.format(en, '%.5f', res.props['top'])].join(';')
        }//.throttleFirst(200, TimeUnit.MILLISECONDS, Schedulers.computation())
    }

    public static void singleSimulation(float popPercencual, float highMutationRate, float lowMutationRate) {
        int SECONDS = 30
        def mediator = new BugMediator<>(popPercencual, highMutationRate, lowMutationRate)
        def bugs = new BugDataSet()
        def instances = ['/kate.csv', '/kate-2.csv', '/kate-3.csv', '/kate-4.csv', '/kate-5.csv',
                         '/kate-6.csv', '/kate-7.csv', '/kate-8.csv', '/kate-9.csv', '/kate-10.csv']

        def changes = Observable.interval(0, SECONDS, TimeUnit.SECONDS).map {
            if (it < instances.size())
                bugs.read(ReflectionUtils.getCallingClass(0).getResourceAsStream(instances[(int) it]))
            else
                new Instance<Bug>([], new BugGenerator())
        }.take(instances.size() + 1)

        def solutions = mediator.mediator(changes)
        def initial = System.currentTimeMillis()
        def obs = solutions.map { Result<Bug> res ->
            [value: res.value, state: res.state*.asMap(), mean: res.props['mean'], std: res.props['std'],
             pop  : res.props['pop'], top: res.props['top'], time: (System.currentTimeMillis() - initial) / 1000]
        }.throttleFirst(1, TimeUnit.SECONDS, Schedulers.io())

        new MainChart(obs)
    }

    public static void mediator(ChangeType type) {
        int seconds = 30
        HazelcastInstance hazelcast = Hazelcast.newHazelcastInstance()
        BugMediator<Integer> mediator = new BugMediator<>()
        DataSet<Bug> bugs = new BugDataSet()
        InputStream is = Main.classLoader.getResourceAsStream('./kate.csv')
        DynamicInstance<Bug> dyn = new DynamicInstance<Bug>(bugs.read(is), type)

        ITopic topic = hazelcast.getTopic('bugs-topic')
        def solutions = mediator.mediator(dyn.makeObservable(seconds))
        def initial = System.currentTimeMillis()
        def sub = solutions.map { Result<Bug> res ->
            [value: res.value, state: res.state*.asMap(), mean: res.props['mean'],
             pop  : res.props['pop'], top: res.props['top'], time: (System.currentTimeMillis() - initial) / 1000]
        }.throttleFirst(1, TimeUnit.SECONDS, Schedulers.io()).subscribe({
            topic.publish(it)
        })

        def scan = new Scanner(System.in)
        while (true) {
            println 'Digite "quit" para parar execução'
            def msg = scan.nextLine()
            if (msg.trim() == 'quit') {
                sub.unsubscribe()
                break
            }
        }
        scan.close()
    }

    public static void optimize() {
        DataSet<Bug> bugs = new BugDataSet()
        InputStream is = Main.classLoader.getResourceAsStream('./kate.csv')
        Instance<Bug> instance = bugs.read(is)

        def hazelcast = Hazelcast.newHazelcastInstance()

        Solver solver = new GASolver<Bug>(instance, 30)
        def obs = solver.solve().map {
            [value: it.value, state: it.state*.asMap(), properties: it.props]
        }.distinct()

        def topic = hazelcast.getTopic('bugs-topic')

        //Exibindo novas instancias no exato momento que elas foram geradas
        def sub = obs.subscribe({
            topic.publish(it)
        }, { err -> println "ERROR: ${err.message}" })

        def scan = new Scanner(System.in)
        while (true) {
            def msg = scan.nextLine()
            if (msg.trim() == 'quit') {
                sub.unsubscribe()
                break
            }
        }
        scan.close()
        hazelcast.shutdown()
        System.exit(0)
    }

    public static void usage(ChangeType type, List<Integer> periodArgs) {
        //Generator<Bug> gen = new BugGenerator()
        //Instance<Bug> bugs = gen.generateInstance(3)
        DataSet<Bug> bugs = new BugDataSet()
        InputStream is = Main.classLoader.getResourceAsStream('./kate.csv')
        DynamicInstance<Bug> dyn = new DynamicInstance<Bug>(bugs.read(is), type)

        def instance = Hazelcast.newHazelcastInstance()
//        def queue = instance.getQueue('bugs-queue')
        def topic = instance.getTopic('bugs-topic')

        //Gera observavel com as modificações a serem efetuadas na faixa de tempo determinada.
        def obs = dyn.makeObservable(*periodArgs)

        //Exibindo novas instancias no exato momento que elas foram geradas
        def sub = obs.subscribe {
            List<Map> data = it.data*.asMap()
//            queue.offer(data)
            topic.publish(data)
        }

        synchronized (this) {
            this.wait()
        }
        addShutdownHook { sub.unsubscribe() }
    }
}
