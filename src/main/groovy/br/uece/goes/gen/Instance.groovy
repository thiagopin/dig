package br.uece.goes.gen

import groovy.transform.EqualsAndHashCode
import groovy.transform.PackageScope
import groovy.transform.TypeChecked
import org.pcollections.PVector
import org.pcollections.TreePVector

@TypeChecked
@EqualsAndHashCode
class Instance<T> {
    private PVector<T> data
    private Generator<T> gen

    public Instance(List<T> data, Generator<T> gen) {
        this.data = TreePVector.from(data)
        this.gen = gen
    }

    @PackageScope
    Instance<T> put(T instance, int at) {
        if (at < 0 || at >= this.data.size()) this
        else new Instance<T>(this.data[0..<at] + instance + this.data[at..-1], this.gen)
    }

    @PackageScope
    Instance<T> remove(int at) {
        if (at < 0 || at >= this.data.size()) this
        else if (at == this.data.size() - 1) new Instance<T>(this.data.init(), this.gen)
        else new Instance<T>(this.data[0..<at] + this.data[at + 1..-1], this.gen)
    }

    @PackageScope
    Instance<T> mutate(T instance, int at) {
        if (at < 0 || at >= this.data.size()) this
        else if (at == this.data.size() - 1) new Instance<T>(this.data.init() + [instance], this.gen)
        else new Instance<T>(this.data[0..<at] + instance + this.data[at + 1..-1], this.gen)
    }

    /**
     * Adiciona novo elemento ao final da instância.
     * @param elem elemento a ser adicionado.
     * @return Instância modificada.
     */
    public Instance<T> append(T elem) {
        new Instance<T>(this.data + elem, this.gen)
    }

    /**
     * Aumenta a instância em um elemento numa posição aleatória.
     * @return Instância modificada.
     */
    public Instance<T> increaseInstance() {
        if (data.isEmpty()) append(gen.generate())
        else put(gen.generate(), (int) (0..<this.data.size()).rand())
    }

    public Instance<T> increaseInstance(int numElements) {
        (0..<numElements).inject(this) { acc, val -> acc.increaseInstance() }
    }

    /**
     * Diminui a instância em um elemento numa posição aleatória. Caso a instância esteja vazia, retorna ela mesma.
     * @return Instância modificada.
     */
    public Instance<T> decreaseInstance() {
        if (data.isEmpty()) this
        else remove((int) (0..<this.data.size()).rand())
    }

    public Instance<T> decreaseInstance(int numElements) {
        (0..<numElements).inject(this) { acc, val -> acc.decreaseInstance() }
    }

    /**
     * Altera um elemento aleatório da instância. Caso a instância esteja vazia, retorna ela mesma.
     * @return Instância modificada.
     */
    public Instance<T> mutateInstance() {
        if (data.isEmpty()) this
        else {
            int idx = (int) (0..<this.data.size()).rand()
            mutate(gen.update((T) this.data.get(idx)), idx)
        }
    }

    public Instance<T> mutateInstance(int numElements) {
        (0..<numElements).inject(this) { acc, val -> acc.mutateInstance() }
    }

    @Override
    String toString() {
        data.toString()
    }

    /**
     * Converte a instancia em formato de lista.
     * @return dados da instância.
     */
    public List<T> getData() {
        data as List<T>
    }

    /**
     * Retorna o número de elementos nesta instância.
     * @return número de elementos nesta instância.
     */
    public int size() {
        data.size()
    }
}
