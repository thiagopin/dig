package br.uece.goes.desp.ga

import br.uece.goes.sample.Bug
import groovy.transform.CompileStatic

@CompileStatic
class BugEvaluator {

    private List<Bug> bugs
    private List<Double> votes
    private double alpha, beta, gamma

    public BugEvaluator(List<Bug> bugs, Double alpha, Double beta, Double gamma) {
        this.bugs = bugs.asImmutable()
        this.votes = normVotes(bugs)
        this.alpha = alpha
        this.beta = beta
        this.gamma = gamma
    }

    public BugEvaluator(List<Bug> bugs) {
        this(bugs, 1.0 as Double, 1.0 as Double, 1.0 as Double)
    }

    List<Double> normVotes(List<Bug> bugs) {
        Bug min = bugs.min { it.votes }
        Bug max = bugs.max { it.votes }
        bugs.collect { ((it.votes - min.votes) / (max.votes - min.votes)).doubleValue() }
    }

    public double bugScore(Bug bug, int pos) {
        alpha * votes[pos] + beta * (bug.priority * (bugs.size() - pos + 1)) - gamma * (bug.severity * pos)
    }

    double getFitness(List<Integer> candidate) {
        return candidate.inject(0.0) { acc, pos ->
            double total = bugScore(bugs[pos], pos)
            acc += total >= 0 ? total : 0
        } as Double
    }

}
