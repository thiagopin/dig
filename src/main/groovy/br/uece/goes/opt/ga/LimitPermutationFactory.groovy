package br.uece.goes.opt.ga

import groovy.transform.CompileStatic
import org.uncommons.watchmaker.framework.factories.AbstractCandidateFactory

@CompileStatic
public class LimitPermutationFactory<T> extends AbstractCandidateFactory<List<T>> {
    private final List<T> elements;
    private final int size;

    public LimitPermutationFactory(List<T> elements, int size) {
        this.elements = elements;
        this.size = size;
    }

    public List<T> generateRandomCandidate(Random rng) {
        return elements.rand(size, false, rng)
    }
}
