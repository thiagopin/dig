package br.uece.goes.gen

import groovy.transform.TypeChecked

@TypeChecked
public abstract class Generator<T> {

    /**
     * Método abstrato para gerar um indivíduo.
     * @return O indivíduo gerado.
     */
    public abstract T generate()

    public abstract T update(T elem)

    /**
     * Método responsável em criar uma população de indivíduos.
     * @param size tamanho da população a ser criada (padrão: 100).
     * @return População de individuos.
     */
    public Instance<T> generateInstance(int size = 100) {
        new Instance<T>((0..<size).collect { generate() }, this)
    }

}