# DIG (Dynamic Instance Generator)

Licensed under [Apache License, Version 2.0](http://www.apache.org/licenses/LICENSE-2.0).

## Execução via código fonte
Para executar o trabalho a partir do código fonte deve-se seguir os seguintes passos:

* Instalar o git.
* Instalar o `Java 8`.
* Clonar o repositório com o código fonte.
* Executar o comando gradle de execução da solução.

Os passos podem ser reproduzidos pelos seguintes comandos (via terminal):

    git clone https://thiagopin@bitbucket.org/thiagopin/dig.git
    cd dig
    ./gradlew run -PpopPerc=0.1 -PhighMut=0.6 -PlowMut=0.05 #(para linux e mac)
    gradlew.bat run -PpopPerc=0.1 -PhighMut=0.6 -PlowMut=0.05 #(para windows)

Onde o parâmetro `-PpopPerc` é o percentual da população que deverá ser aproveitado no momento
da mudança na instância (no exemplo acima, indica que 10% dos melhores individuos de uma população, 
serão usados na população da nova execução). Já o parâmetro `-PhighMut` indica a taxa de mutação 
inicial da Hypermutação e o parâmetro `-PlowMut` indica a taxa de mutação final.


### Dicas: 
* Para executar uma simulação onde nenhum elemento da população anterior seja reaproveitado
na execução seguinte o parâmetro `-PpopPerc=0.0` pode ser utilizado, já no caso de todos os elementos
serem reutilizados (ou seja, não fazer uso de Imigrantes Aleatórios) utilizar o parâmetro `-PpopPerc=1.0`.

* Uma maneira de fazer uso ou não da Hypermutação é simplesmente ajustar os parâmetros de mutação, no 
exemplo acima os parâmetros `-PhighMut=0.6` e `-PlowMut=0.05` indicam que a taxa de mutação será iniciada
em 60% e decairá para 5% após cada modificação na instância (a primeira execução, anterior a qualquer
mudança permanecerá em 5%). Portanto para evitar o usa da hypermutação e um operador de mutação tradicional
os parâmetros iniciais e finais podem ser o mesmo, `-PhighMut=0.05` e `-PlowMut=0.05` implicaria que a mutação
sempre será de 5% em todas as execuções.

## Acknowledgement

### GOES - Opitimization in Software Engineering Group


[![alt text](http://goes.uece.br/style/images/logo_goes.png "GOES")](http://www.goes.uece.br)

[Apache License, Version 2.0](http://www.apache.org/licenses/LICENSE-2.0)