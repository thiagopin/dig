package br.uece.goes.opt;

import rx.Observable;

import java.util.Map;

public interface Solver<T> {

    Observable<Result<T>> solve(Map<String, ? extends Object> params);
}