package br.uece.goes.desp

import br.uece.goes.gen.Instance
import br.uece.goes.opt.Result
import br.uece.goes.opt.ga.*
import br.uece.goes.opt.generator.GaussGenerator
import br.uece.goes.sample.Bug
import groovy.transform.TypeChecked
import org.uncommons.maths.random.MersenneTwisterRNG
import org.uncommons.maths.random.Probability
import org.uncommons.watchmaker.framework.PopulationData
import org.uncommons.watchmaker.framework.TerminationCondition
import org.uncommons.watchmaker.framework.operators.EvolutionPipeline
import org.uncommons.watchmaker.framework.operators.ListOrderCrossover
import org.uncommons.watchmaker.framework.operators.Replacement
import org.uncommons.watchmaker.framework.selection.RouletteWheelSelection
import org.uncommons.watchmaker.framework.termination.ElapsedTime

@TypeChecked
class GADesp<T> {

    private final Instance<T> instance
    private final int numBugs = 30
    private final int fase
    private final List<List<Integer>> seed

    private final float ALPHA = 1.0
    private final float BETA = 1.0
    private final float GAMMA = 1.0

    public GADesp(Instance<T> instance, int fase) {
        this(instance, [], fase)
    }

    public GADesp(Instance<T> instance, List<List<Integer>> seed, int fase) {
        this.instance = instance
        this.seed = seed
        this.fase = fase
    }

    private double topScore(BugEvaluator evaluator, List<Bug> bugs) {
        (double) (0..<bugs.size()).collect { Integer it -> evaluator.bugScore(bugs[it], it) }
                .sort(false) { double it -> -it }.take(numBugs).sum()
    }

    private void createSubscription(double alpha, double beta, double gamma,
                                    double highMutationRate, double lowMutationRate, long secs, Closure callback) {
        Random rng = new MersenneTwisterRNG()
        final TerminationCondition termination = new ElapsedTime(secs * 1000)
        final int populationSize = 100

        def evaluator = new BugEvaluator(instance.data as List<Bug>, alpha, beta, gamma)
        def top = topScore(evaluator, (List<Bug>) instance.data)

        def generator = new GaussGenerator(highMutationRate, lowMutationRate, 4, 0.0002)

        def cFactory = new LimitPermutationFactory<>((0..<instance.size()), numBugs)

        def pipeline = new EvolutionPipeline([
                new ListOrderCrossover<Integer>(),
                new BugMutation((0..<instance.size()) as IntRange, generator),
                new Replacement<List<Integer>>(cFactory, new Probability(0.15))])

        def engine = new PopGenerationalEvolutionEngine<>(cFactory, pipeline,
                evaluator, new RouletteWheelSelection(), rng)
        engine.singleThreaded = true

        engine.addPopEvolutionObserver(new PopEvolutionObserver<List<Integer>>() {
            @Override
            void populationUpdate(PopulationData<? extends List<Integer>> data, List<? extends List<Integer>> pop) {
                generator.increment()
                def result = new Result<T>(BigDecimal.valueOf(data.bestCandidateFitness),
                        data.bestCandidate.collect { Integer el -> instance.data[el] },
                        [pop : pop, mean: data.meanFitness, std: data.fitnessStandardDeviation, top: top,
                         fase: fase, gen: data.generationNumber])
                callback(result)

            }
        })

        engine.evolve(populationSize, 1, seed, termination)
    }

    public void solve(double highMutationRate, double lowMutationRate, long time, Closure callback) {
        createSubscription(ALPHA, BETA, GAMMA, highMutationRate, lowMutationRate, time, callback)
    }

}
