package br.uece.goes.sample

import br.uece.goes.dataset.DataSet
import br.uece.goes.gen.Instance

public class BugDataSet implements DataSet<Bug> {

    @Override
    Instance<Bug> read(InputStream is) {
        List<Bug> bugs = is.readLines('utf-8').tail().collect {
            List<String> data = it.split(';')
            new Bug(id: data[0].toLong(), votes: data[1].toInteger(),
                    priority: data[2].toDouble(), severity: data[3].toDouble())
        }
        new Instance<Bug>(bugs, new BugGenerator(counter: (bugs*.id).max() + 1))
    }
}
