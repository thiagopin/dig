package br.uece.goes.dataset;

import br.uece.goes.gen.Instance;

import java.io.InputStream;

public interface DataSet<T> {

    Instance<T> read(InputStream is);
}